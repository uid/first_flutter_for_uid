import 'package:flutter/material.dart';

//import './demo/btn_click.dart';
//import './demo/tarbar.dart';
import './demo/layout_column.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:path_provider/path_provider.dart';

void main() => runApp(HomeApp());

class HomeApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('en', 'US'),
        const Locale('zh', 'CN'),
      ],
      debugShowCheckedModeBanner: false,
      title: '鱼丸大松狮',
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
      ),
      home: HomePage(
        title: '小短手 大鱼丸',
      ),
    );
  }
}
