import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class HomePage extends StatelessWidget {
  HomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) {
    return new DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          title: Text('丸'),
          leading: new IconButton(
            icon: new Container(
              padding: EdgeInsets.all(3.0),
              child: new CircleAvatar(
                  radius: 30.0,
                  backgroundImage: AssetImage("assets/images/logo.jpg")),
            ),
            onPressed: () {
              Fluttertoast.showToast(
                  msg: "你点到logo了",
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.TOP,
                  timeInSecForIos: 1,
                  backgroundColor: Colors.red,
                  textColor: Colors.white,
                  fontSize: 16.0);
            },
          ),
          actions: <Widget>[
            IconButton(
              icon: new Icon(Icons.search),
              onPressed: () => debugPrint('Search button is pressed.'),
            ),
          ],
        ),
        body: TabBarView(
          children: <Widget>[
            Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    '你再点一下试试？手都给你打断掉:',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 24.0,
                    ),
                  ),
                ],
              ),
            ),
            Text(
              '第二屏',
              style: TextStyle(
                color: Colors.white,
                fontSize: 24.0,
              ),
            ),
            Text(
              '第三屏',
              style: TextStyle(
                color: Colors.white,
                fontSize: 24.0,
              ),
            ),
          ],
        ),
        bottomSheet: TabBar(
          labelColor: Colors.blue,
          unselectedLabelColor: Colors.black54,
          indicatorColor: Colors.black87,
          indicatorSize: TabBarIndicatorSize.label,
          indicatorWeight: 1.0,
          tabs: <Widget>[
            Tab(icon: Icon(Icons.local_florist)),
            Tab(icon: Icon(Icons.change_history)),
            Tab(icon: Icon(Icons.directions_bike)),
          ],
        ),
        backgroundColor: Colors.black12,
      ),
    );
  }
}
