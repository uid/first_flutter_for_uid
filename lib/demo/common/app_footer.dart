import 'package:flutter/material.dart';

class AppFooter extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return new BottomNavigationBar(
      items: <BottomNavigationBarItem>[
        new BottomNavigationBarItem(
          icon: Icon(Icons.local_florist),
          title: Text('Home'),
        ),
        new BottomNavigationBarItem(
          icon: Icon(Icons.change_history),
          title: Text('Catagory'),
        ),
        new BottomNavigationBarItem(
          icon: Icon(Icons.directions_bike),
          title: Text('Profile'),
        ),
      ],
      type: BottomNavigationBarType.fixed,
      //默认选中首页
      currentIndex: 0,
      iconSize: 24.0,
      //点击事件
      onTap: (index) {
        debugPrint('Current tap index:' + index.toString());
      },
    );
  }

}
