import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _BtnClick createState() => _BtnClick();
}

class _BtnClick extends State<HomePage> {
  int _counter = 0;

  void _incrementBtnHandle() {
    _incremenToast();
    _incrementCounter();
  }

  void _incremenToast() {
    Fluttertoast.showToast(
        msg: "还点...调皮...",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.TOP,
        timeInSecForIos: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0
    );
  }

  void _incrementCounter() {
    setState(() {
      ++_counter;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        leading: new IconButton(
          icon: new Container(
            padding: EdgeInsets.all(3.0),
            child: new CircleAvatar(
                radius: 30.0,
                backgroundImage:
                AssetImage("assets/images/logo.jpg")),
          ),
          onPressed: () {
            Fluttertoast.showToast(
                msg: "你点到logo了",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.TOP,
                timeInSecForIos: 1,
                backgroundColor: Colors.red,
                textColor: Colors.white,
                fontSize: 16.0
            );
          },
        ),
        actions: <Widget>[
          IconButton(
            icon: new Icon(Icons.search),
            onPressed: () => debugPrint('Search button is pressed.'),
          ),
        ],
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              '你再点一下试试？手都给你打断掉:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.display1,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementBtnHandle,
        tooltip: '太顽皮了，还点。。。',
        child: Icon(Icons.add),
      ),
    );
  }
}
