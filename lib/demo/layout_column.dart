import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import './common/app_footer.dart';
import './common/app_floating_action_location.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _BtnClick createState() => _BtnClick();
}

class _BtnClick extends State<HomePage> {
  int _counter = 0;

  void _toast(String content, ToastGravity gravity) {
    Fluttertoast.showToast(
        msg: content,
        toastLength: Toast.LENGTH_SHORT,
        gravity: gravity,
        timeInSecForIos: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0);
  }

  void _incrementBtnHandle() {
    _incremenToast();
    _incrementCounter();
  }

  void _incremenToast() {
    _toast("还点...调皮...", ToastGravity.TOP);
  }

  void _incrementCounter() {
    setState(() {
      ++_counter;
    });
  }

  @override
  Widget build(BuildContext context) {
    Column buildButtonColumn(IconData icon, String label) {
      Color color = Theme.of(context).primaryColor;

      return new Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          new Icon(icon, color: color),
          new Container(
            margin: const EdgeInsets.only(top: 8.0),
            child: new Text(
              label,
              style: new TextStyle(
                fontSize: 12.0,
                fontWeight: FontWeight.w400,
                color: color,
              ),
            ),
          ),
        ],
      );
    }

    Widget titleSection = new Container(
      padding: const EdgeInsets.all(32.0),
      child: new Row(
        children: [
          new Expanded(
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                new Container(
                  padding: const EdgeInsets.only(bottom: 8.0),
                  child: new Text(
                    'Oeschinen Lake Campground',
                    style: new TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                new Text(
                  'Kandersteg, Switzerland',
                  style: new TextStyle(
                    color: Colors.grey[500],
                  ),
                ),
              ],
            ),
          ),
          new Icon(
            Icons.star,
            color: Colors.red[500],
          ),
          new Text('41'),
        ],
      ),
    );

    Widget textSection = new Container(
      padding: const EdgeInsets.all(32.0),
      child: new Text(
        '''
Lake Oeschinen lies at the foot of the Blüemlisalp in the Bernese Alps. Situated 1,578 meters above sea level, it is one of the larger Alpine Lakes. A gondola ride from Kandersteg, followed by a half-hour walk through pastures and pine forest, leads you to the lake, which warms to 20 degrees Celsius in the summer. Activities enjoyed here include rowing, and riding the summer toboggan run.
        ''',
        softWrap: true,
      ),
    );

    Widget buttonSection = new Container(
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          buildButtonColumn(Icons.call, 'CALL'),
          buildButtonColumn(Icons.near_me, 'ROUTE'),
          buildButtonColumn(Icons.share, 'SHARE'),
        ],
      ),
    );

    Widget netImageWithTitle(String imgUrl,
        {String title = "图片", int flex = 1}) {
      return Expanded(
        flex: flex,
        child: Stack(
          alignment: const Alignment(0.6, 0.6),
          children: [
            Image.network(
              imgUrl,
              fit: BoxFit.fill,
//              width: 100,
            ),
            new Container(
              decoration: new BoxDecoration(
                color: Colors.black45,
              ),
              child: new Text(
                title,
                style: new TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                ),
              ),
            ),
          ],
        ),
      );
    }

    Widget netImageSection = new Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        netImageWithTitle(
          'http://uid.gitee.io/first_flutter_for_uid/001.jpg',
          title: '大状',
        ),
        netImageWithTitle(
          'http://uid.gitee.io/first_flutter_for_uid/002.jpg',
          title: '流星',
          flex: 3,
        ),
        netImageWithTitle(
          'http://uid.gitee.io/first_flutter_for_uid/003.jpg',
          title: '破丸',
        ),
      ],
    );

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        leading: new IconButton(
          icon: new Container(
            padding: EdgeInsets.all(3.0),
            child: new CircleAvatar(
                radius: 30.0,
                backgroundImage: AssetImage("assets/images/logo.jpg")),
          ),
          onPressed: () {
            _toast("你点到logo了", ToastGravity.TOP);
          },
        ),
        actions: <Widget>[
          IconButton(
            icon: new Icon(Icons.search),
            onPressed: () => debugPrint('Search button is pressed.'),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new IconButton(
//              icon: ImageIcon(AssetImage("assets/images/champion.jpg")),
                icon: new Image.asset(
                  'assets/images/champion.jpg',
                  height: 240.0,
                  fit: BoxFit.fitWidth,
                ),
                iconSize: 240.0,
                onPressed: () => _toast("这是状元.", ToastGravity.TOP),
              ),
              Text(
                '你再点一下试试？手都给你打断掉:',
              ),
              Text(
                '$_counter',
                style: Theme.of(context).textTheme.display1,
              ),
              titleSection,
              buttonSection,
              textSection,
              netImageSection,
            ],
          ),
        ),
        padding: EdgeInsets.only(bottom: 50.0),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementBtnHandle,
        tooltip: '太顽皮了，还点。。。',
        child: Icon(Icons.add),
      ),
      floatingActionButtonLocation: AppFloatingActionLocation(
          FloatingActionButtonLocation.endFloat, 0, -80.0),
      bottomSheet: new AppFooter(),
    );
  }
}
